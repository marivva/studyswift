//
//  Car.swift
//  AutoCatalog
//
//  Created by Masha on 30/01/2022.
//

import Foundation

struct Car: CustomStringConvertible, Equatable {
    private let id: UUID = UUID()
    
    let name: String
    let year: Int
    let model: String
    
    var description: String {
        return """
               Name \(name)
               Year \(year)
               Model \(model)
               """
    }
    
    func isContain(sign: String) -> Bool {
        if model.contains(sign) || name.contains(sign) || String(year).contains(sign) {
            return true
        } else {
            return false
        }
    }
    
    static func ==(lhs: Car, rhs: Car) -> Bool {
        return lhs.id == rhs.id
    }
}

