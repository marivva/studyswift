//
//  Command.swift
//  AutoCatalog
//
//  Created by Masha on 30/01/2022.
//

enum Command: String, CaseIterable {
    case print = "print"
    case add = "add"
    case removeAll = "rm all"
    case removeIndex = "rm ind"
    case removeSign = "rm sign"
    case exit = "exit"
    
    static let commands: [Command] = Command.allCases
}
