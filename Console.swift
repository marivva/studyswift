//
//  Console.swift
//  AutoCatalog
//
//  Created by Masha on 30/01/2022.
//

class Console {
    private let storage: Storage
    
    init(storage: Storage) {
        self.storage = storage
    }
    
    func run() {
        var isWorked: Bool = true
        while isWorked {
            print("Write command:", separator: "", terminator: "")
            guard let commandOfStr = readLine() else {
                fatalError("Ooops...")
            }
            guard let command = Command(rawValue: commandOfStr) else {
                print("Please write correct command: [\(allCommandsOfStr())]")
                continue
            }
            
            switch command {
            case .exit:
                isWorked = false
            case .print:
                printCarList()
            case .add:
                addCar()
            case .removeAll:
                removeAllCars()
            case .removeIndex:
                removeCarByIndex()
            case .removeSign:
                removeCarBySign()
            }
        }
    }
    
    private func allCommandsOfStr() -> String {
        var result: String = ""
        for command in Command.commands {
            result += "'\(command.rawValue)' "
        }
        return result
    }
    
    private func carListIsEmpty() -> Bool {
        if storage.cars.isEmpty {
            print("List is empty")
            return true
        } else {
            return false
        }
    }
    
    private func printCarList() {
        if carListIsEmpty() {
            return
        }
        
        for (i, car) in storage.cars.enumerated() {
            print("#", i + 1, separator: "")
            print(car)
        }
    }

    private func addCar() {
        print("Write car name: ", separator: "", terminator: "")
        guard let carName = readLine() else {
            fatalError("Ooops...")
        }
        print("Write car year: ", separator: "", terminator: "")
        
        var carYear: Int = 0
        while true {
            guard let carYearOfStr = readLine(), let newCarYear = Int(carYearOfStr) else {
                print("Please write correct year")
                continue
            }
            
            carYear = newCarYear
            break
        }
        
        print("Write car model: ", separator: "", terminator: "")
        guard let carModel = readLine() else {
            fatalError("Ooops...")
        }

        storage.addCar(Car(name: carName, year: carYear, model: carModel))
    }

    private func removeCarByIndex() {
        if carListIsEmpty() {
            return
        }
        
        print("Write car index: ", separator: "", terminator: "")
        var carIndex: Int = -1
        while true {
            guard let carIndexOfStr = readLine(), let newCarIndex = Int(carIndexOfStr), storage.isValidIndex(newCarIndex) else {
                print("Please write correct index: ", separator: "", terminator: "")
                continue
            }
            
            carIndex = newCarIndex - 1
            break
        }
        storage.removeCar(index: carIndex)
    }
    
    private func removeCarBySign() {
        if carListIsEmpty() {
            return
        }
        
        print("Please write sign: ", separator: "", terminator: "")
        guard let carSign = readLine() else {
            fatalError("Ooops...")
        }
        storage.removeCar(with: carSign)
    }
    
    private func removeAllCars() {
        if carListIsEmpty() {
            return
        }
        
        storage.removeAllCars()
    }
}
