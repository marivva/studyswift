//
//  Storage.swift
//  AutoCatalog
//
//  Created by Masha on 30/01/2022.
//

class Storage {
    static let carsMock: [Car] = [Car(name: "car1", year: 5, model: "tutu"), Car(name: "gala", year: 20, model: "soska"),
                                  Car(name: "reruuu", year: 15, model: "tutu"), Car(name: "maha", year: 21, model: "yyyy"),
                                  Car(name: "hhhhh", year: 1, model: "uuuuu")]
    
    private(set) var cars: [Car] = carsMock
    
    func addCar(_ car: Car) {
        cars.append(car)
    }
    
    func removeCar(index: Int) {
        cars.remove(at: index)
        print("#\(index + 1) car removed successfuly")
    }
    
    func removeCar(car carForRemove: Car) {
        cars.removeAll { car in
            return car == carForRemove
        }
    }
    
    func removeCar(with sign: String) {
        cars.removeAll { car in
            return car.isContain(sign: sign)
        }
        print("Car(s) with \"\(sign)\" removed successfuly")
    }
    
    func removeAllCars() {
        cars.removeAll()
        print("Cars removed successfuly")
    }
    
    func isValidIndex(_ carIndex: Int) -> Bool {
        if cars.isEmpty || carIndex <= 0 || carIndex > cars.endIndex {
            return false
        } else {
            return true
        }
    }
}
